import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.Keys
import org.stringtemplate.v4.compiler.STParser.element_return

WebUI.setText(findTestObject('Object Repository/Page_invenio/input_Archivsignatur_masterLayoutFormtabSearchtabSearchAllgemeinarchSignature'), 
   
	 code.replaceAll("([A-Za-z0-9]+)_([A-Za-z0-9]+)_([A-Za-z0-9]+)_([0-9]+)_?([a-zA-Z]*)",'$1 $2-$3/$4$5').replaceAll("([0-9a-zA-Z]+)_([0-9a-zA-Z]+)_([0-9a-zA-Z]+)_?([a-zA-Z]*)",'$1 $2/$3$4'))
WebUI.delay(1)
WebUI.click(findTestObject('Object Repository/Page_invenio/input_Aufbewahrungsfrist_masterLayoutFormtabSearchtabSearchAllgemeinsearchDoSearch'))


/* der eigentliche Test */
try {
	
	/*	
	WebUI.verifyElementVisible(findTestObject('Object Repository/Page_invenio/h2_Die Treffer zu Ihrer Suche wurden ermittelt. Sie finden sie in der Tektonik.'))
	this.println(code + ' liefert Ergebnisse in der Tektonik')
	boolean unsichtbar= true

	while ( unsichtbar ) {
		WebUI.click(findTestObject('Object Repository/Page_invenio/tektonikTreffer'))
		WebUI.delay(1)
		try {
			unsichtbar= WebUI.verifyElementNotVisible(findTestObject('Object Repository/Page_invenio/h2_Treffer in der Klassifikation'))
		}
		catch (Exception regulaeresEndeTektonik) {
			unsichtbar= false
		}
	}
	String div= 'div/'
	String xstart= "//div[@id='klassifikationPanel']/div/div/" + div 
	String xtail= "div/div/span/span/span[@title='Trefferanzahl']"
	String xpath= "" 
	boolean clickbar= true
	while (clickbar){
		xpath= xstart + xtail
		TestObject to= new TestObject().addProperty('xpath', ConditionType.EQUALS, xpath)
		WebUI.delay(1)
		try {
			// WebUI.verifyElementNotVisible(findTestObject('Object Repository/Page_invenio/Merken_Bestellen'))
			WebUI.click(to)
			xstart+= div
		}
		catch (Exception regulaeresEndeKlassifikation) {
			clickbar= false
		}
	}
	*/
}
catch (Exception nichtsGefunden) {
// es wurde wohl nichts gefunden

	WebUI.click(findTestObject('Object Repository/Page_invenio/a_OK'))
	this.println(code + ' wurde nicht gefunden')
	throw new com.kms.katalon.core.exception.StepFailedException(code +' nicht vorhanden')

}
if (WebUI.verifyElementClickable(findTestObject('Object Repository/Page_invenio/a_Digitalisat anzeigen'))) {
	this.println(code + ' liefert ein Digitalisat')
	WebUI.click(findTestObject('Object Repository/Page_invenio/a_Digitalisat anzeigen'))
	
	if (! WebUI.verifyElementClickable(findTestObject('Object Repository/Page_invenio/p_Das Digitalisat ist nicht zur Anzeige im Internet freigegebenEine Anzeige im Benutzersaal des Bundesarchivs ist nur nach vorhergehender Berechtigungsprfung mglich'), FailureHandling.OPTIONAL))
	{
	    // WebUI.switchToWindowIndex(1, FailureHandling.STOP_ON_FAILURE)
		WebUI.switchToWindowTitle('', FailureHandling.STOP_ON_FAILURE)
			//WebUI.switchToWindowTitle('')
		try{
			WebUI.delay(5)
			WebUI.click(findTestObject('Object Repository/Page_/div_Archivsignatur B 3231_BRnavright'))
			//WebUI.click(findTestObject('Object Repository/Page_/div_Seite 395'))
			
			//WebUI.getText(findTestObject('Object Repository/Page_/div_Seite 395'))
			
			WebUI.sendKeys(findTestObject('Object Repository/Page_/a_Seite 395'), Keys.chord(Keys.END))
			WebUI.getAttribute(findTestObject('Object Repository/Page_/a_Seite 395'),'style')
			WebUI.click(findTestObject('Object Repository/Page_/div_Seite 395'))
			
			WebUI.getText(findTestObject('Object Repository/Page_/div_Seite 395'))
			WebUI.click(findTestObject('Object Repository/Page_/a_Seite 395'))
			WebUI.getText(findTestObject('Object Repository/Page_/a_Seite 395'))
			
			WebUI.delay(5)
			
			WebUI.closeWindowTitle('')
			WebUI.switchToWindowTitle('invenio')
		}
		catch(Exception  digitalisatNichtMehrseitig){
			WebUI.delay(1)
			try {
				WebUI.click(findTestObject('Object Repository/Page_/following_button3'))
			} catch(Exception digitalisatNichtEinseitig) {
			
				WebUI.closeWindowTitle('')
				WebUI.switchToWindowTitle('invenio')
				throw new com.kms.katalon.core.exception.StepFailedException(code +' nicht als Digitalisat vorhanden')
			}
			WebUI.closeWindowTitle('')
			WebUI.switchToWindowTitle('invenio')
			this.println(code + " hatte ein Digitalisat")
		}
	} else {
		WebUI.click(findTestObject('Object Repository/Page_invenio/a_Abbrechen'))
		throw new com.kms.katalon.core.exception.StepFailedException(code +' nicht als Digitalisat zugaenglich') 
	}
}
