
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.SimpleDateFormat

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.SetupTestCase
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.annotation.TearDownTestCase
import groovy.time.*

/**
 * Some methods below are samples for using SetUp/TearDown in a test suite.
 */

/**
 * Setup test suite environment.
 */
@SetUp(skipped = false) // Please change skipped to be false to activate this method.
def setUp() {
	WebUI.openBrowser('')
    
	WebUI.navigateToUrl('https://invenio.bundesarchiv.de/basys2-invenio/login.xhtml')
	WebUI.refresh()
	
	WebUI.click(findTestObject('Object Repository/Page_invenio - Anmeldung/span_invenio_base-bigtarget'))
	
	WebUI.click(findTestObject('Object Repository/Page_invenio/a_Schlieen'))

	//WebUI.waitForElementVisible(findTestObject('Object Repository/Page_invenio/span_Suche'),15)
	int loop= 0
	int fehler= 0
	while (fehler == loop && loop <= 5) {
		try {
			WebUI.click(findTestObject('Object Repository/Page_invenio/span_Suche'))
		}
		catch (Exception e) {
			fehler++
			WebUI.delay(1)
		}
		loop++
	}
	//WebUI.click(findTestObject('Object Repository/Page_invenio/span_weitere Filter hinzufgen'))

}

/**
 * Clean test suites environment.
 */
@TearDown(skipped = false) // Please change skipped to be false to activate this method.
def tearDown() {
	WebUI.closeBrowser()
}

/**
 * Run before each test case starts.
 */
@SetupTestCase(skipped = true) // Please change skipped to be false to activate this method.
def setupTestCase() {
	// Put your code here.
}

/**
 * Run after each test case ends.
 */
@TearDownTestCase(skipped = false) // Please change skipped to be false to activate this method.
def tearDownTestCase() {
	int wartungstart= 1945   
	int wartungende = 2005    
	Date now = new Date()    
	Calendar c = Calendar.getInstance()    
	c.setTime(now)    
	int t = c.get(Calendar.HOUR_OF_DAY) * 100 + c.get(Calendar.MINUTE)    
	boolean isBetween = wartungende > wartungstart && t >= wartungstart && t <= wartungende
	if (isBetween) {
		WebUI.delay(5*60)
		WebUI.navigateToUrl('https://invenio.bundesarchiv.de/basys2-invenio/login.xhtml')
		
		WebUI.click(findTestObject('Object Repository/Page_invenio - Anmeldung/span_invenio_base-bigtarget'))
		try {
		WebUI.click(findTestObject('Object Repository/Page_invenio/a_Schlieen'))
		}
		catch (Exception keinSliessenNoetig) {
			
		}
		
		WebUI.waitForElementVisible(findTestObject('Object Repository/Page_invenio/span_Suche'),15)
		int loop= 0
		int fehler= 0
		while (fehler == loop && loop <= 5) {
			try {
				WebUI.click(findTestObject('Object Repository/Page_invenio/span_Suche'))
			}
			catch (Exception e) {
				fehler++
				WebUI.delay(1)
			}
			loop++
		}
		try {
		WebUI.click(findTestObject('Object Repository/Page_invenio/span_weitere Filter hinzufgen'))
		}
		catch (Exception weiterFilterBereitsDa) {}
	
	}
	WebUI.delay(1)
	
	try {
		WebUI.click(findTestObject('Object Repository/Page_invenio/span_Suche'))
		
		WebUI.click(findTestObject('Object Repository/Page_invenio/input_Aufbewahrungsfrist_masterLayoutFormtabSearchtabSearchAllgemeinj_idt1512'))
		
		WebUI.click(findTestObject('Object Repository/Page_invenio/input_Mchten Sie die bereits eingegebenen Parameter wirklich lschen_masterLayoutFormconfirmOk_2'))
	} catch (Exception keinAusgagngszustand) {
		WebUI.closeBrowser()
		setUp()
	}






}

/**
 * References:
 * Groovy tutorial page: http://docs.groovy-lang.org/next/html/documentation/
 */